package com.tsystems.javaschool.tasks.calculator;


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

class ConverterFromInfixToPostfix  {
    private Stack<String> stackOfOperators = new Stack<>();
    private List<String> output = new ArrayList<>();

    /**
     * Change form of math expression.
     * @param inputListOfValues List of Strings with operators and operands in infix form
     * @return List of Strings in postfix form
     */
    public List<String> convertFromInfixToPostfix(List<String> inputListOfValues) {
        for (String element: inputListOfValues) {
            switch (element) {
                case "+":
                case "-":
                    addOperator(element, 1);
                    break;
                case "*":
                case "/":
                    addOperator(element, 2);
                    break;
                case "(":
                    stackOfOperators.push(element);
                    break;
                case ")":
                    addCloseParenthesis();
                    break;
                default:
                    output.add(element);
                    break;
            }
        }
        while (!stackOfOperators.isEmpty()) {
            output.add(stackOfOperators.pop());
        }
        return output;
    }

    private void addOperator(String inputOperator, int priority) {
        while (!stackOfOperators.isEmpty()) {
            String staksOperator = stackOfOperators.pop();

            if (staksOperator.equals("(")) {
                stackOfOperators.push(staksOperator);
                break;
            } else {
                int priorityOfStackOperator;

                if (staksOperator.equals("+") || staksOperator.equals("-")) priorityOfStackOperator = 1;
                else priorityOfStackOperator = 2;

                if (priorityOfStackOperator < priority) {
                    stackOfOperators.push(staksOperator);
                    break;
                } else {
                    output.add(staksOperator);
                }
            }
        }
        stackOfOperators.push(inputOperator);
    }

    private void addCloseParenthesis()  {
        while (!stackOfOperators.isEmpty()) {
            String extractedOperator = stackOfOperators.pop();
            if (extractedOperator.equals("("))
                break;
            else
                output.add(extractedOperator);
        }
    }
}
