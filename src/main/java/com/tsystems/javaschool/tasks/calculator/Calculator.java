package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) return null;

//      Split statement into parts
        List<String> splittedStatement = splitStatement(statement);

//      Conversion from infix to postfix form
        ConverterFromInfixToPostfix converter = new ConverterFromInfixToPostfix();
        List<String> infixStatement = converter.convertFromInfixToPostfix(splittedStatement);

//      Calculation the result
        double doubleResult;
        try {
            doubleResult = calculateInfixStatement(infixStatement);
        } catch (Exception e) {
            return null;
        }

//      Transformation the double result to String
        String result;
        if (doubleResult == (long) doubleResult)
            result = String.format("%d", (long) doubleResult);
        else
            result = String.format("%s", doubleResult);

        return result;
    }

//    public static void main(String[] args) {
//        String example = "10/2-7+3*4";
//        System.out.println(example.matches("[0-9-*/]+"));
//    }

    private List<String> splitStatement(String statement) {
        List<String> result = new ArrayList<>();
        int lengthString = statement.length();
        int startPosition = -1;
        int endPosition = -1;
        for (int index = 0; index < lengthString; index++) {
            char buffer = statement.charAt(index);
            switch (buffer) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    if (startPosition == -1)
                        startPosition = index;
                    endPosition = index;
                    break;
                case '(':
                case ')':
                case '+':
                case '-':
                case '/':
                case '*':
                    endPosition = index - 1;
                    if (startPosition != -1)
                        result.add(statement.substring(startPosition, endPosition + 1));
                    startPosition = -1;
                    result.add(String.valueOf(buffer));
                    break;
            }
        }
        if (startPosition != -1)
            result.add(statement.substring(startPosition, endPosition + 1));
        return result;
    }

    private double calculateInfixStatement(List<String> postfixStatement) {
        Stack<Double> stackOfOpeands = new Stack();

        double number1, number2, interAns;

        for (String buffer: postfixStatement) {
            if (!buffer.equals("+")
                    && !buffer.equals("-")
                    && !buffer.equals("/")
                    && !buffer.equals("*")) {
                Double buffer2 = Double.valueOf(buffer);
                stackOfOpeands.push(buffer2);
            } else {
                number2 = stackOfOpeands.pop();
                number1 = stackOfOpeands.pop();
                switch (buffer) {
                    case "+":
                        interAns = number1 + number2;
                        break;
                    case "-":
                        interAns = number1 - number2;
                        break;
                    case "*":
                        interAns = number1 * number2;
                        break;
                    case "/":
                        if (number2 == 0) throw new ArithmeticException();
                        interAns = number1 / number2;
                        break;
                    default:
                        interAns = 0;
                }
                stackOfOpeands.push(interAns);
            }

        }
        interAns = stackOfOpeands.pop();
        return interAns;
    }
}
