package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {
    private static final int MAX_SIZE_FOR_LIST_INPUT = 1000;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

//      Validate inputNumbers
        if (inputNumbers == null) throw new CannotBuildPyramidException();
        if (inputNumbers.size() == 0) throw new CannotBuildPyramidException();
        if (inputNumbers.size() > MAX_SIZE_FOR_LIST_INPUT) throw new CannotBuildPyramidException();
        for (Integer number: inputNumbers) {
            if (number == null) throw  new CannotBuildPyramidException();
        }

//      Compute supporting variables
        int heightOfMatrix = getHeightOfMatrix(inputNumbers.size());
        int widthOfMatrix = getWidthOfMatrix(heightOfMatrix);
        int coefficientOffset = heightOfMatrix - 1;

//      Create matrix and fill zeros
        int[][] result = new int[heightOfMatrix][widthOfMatrix];
        result = fillMatrixByZero(result, heightOfMatrix, widthOfMatrix);

//      Add sorted values into matrix
        Collections.sort(inputNumbers);
        Iterator iterator = inputNumbers.iterator();
        for (int y = 0; y < heightOfMatrix; y++) {
            for (int x = coefficientOffset; x < widthOfMatrix - coefficientOffset; x += 2) {
                result[y][x] = (int) iterator.next();
            }
            coefficientOffset--;
        }
        return result;
    }

    private int getHeightOfMatrix(int size) {
        int result;
        for (int n = 1;; n++) {
            int triangle = n * (n + 1) / 2;

            if (size == triangle) {
                result = n;
                break;
            }
            if (size < triangle) {
                throw new CannotBuildPyramidException();
            }
        }
        return result;
    }

    private int getWidthOfMatrix(int heightOfMatrix) {
        return heightOfMatrix + heightOfMatrix - 1;
    }

    private int[][] fillMatrixByZero(int[][] input, int height, int width) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                input[i][j] = 0;
            }
        }
        return input;
    }
}
