package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    public boolean find(List x, List y) {
        if (x == null || y == null) throw  new IllegalArgumentException();

        if (x.isEmpty()) return true;

        if (y.isEmpty()) return false;

        boolean result = false;

        int pointerX = 0;

        for (int pointerY = 0; pointerY < y.size(); pointerY++) {
            if (x.get(pointerX).equals(y.get(pointerY))) {
                pointerX++;
                if (pointerX == x.size()) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
}
